package com.dano.app.aa.ui;

import java.awt.CardLayout;
import java.text.ParseException;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import com.dano.app.aa.component.alert;
import com.dano.app.aa.component.app_component;
import com.dano.app.lib.DATE;

public class Main extends app_component {
	public Main() {
		super();
		setTitle("Automate Tool V1.0 [Donate momo - 0905286027]");
		setSize(470, 300);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		setJMenuBar(getMenus());
		contentPane = new JPanel();
		contentPane.setLayout(new CardLayout());
		contentPane.add(new index_main(), "main");
		setContentPane(contentPane);
		addSubMenu(0, "Giao diện chính", new index_main());
		addSubMenu(2, "Đăng Nhập", new LoginPanel());
		showPanel("main");

	}

	public void setContent() {
		setContentPane(contentPane);
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				DATE date = new DATE();
				Main myApp = new Main();
				try {
					if (date.checkTimeEvent("2023-09-19 23:59:59", "2023-09-30 23:59:59")) {
						myApp.setVisible(true);
					} else {
						alert.showErr("Phiên bản dùng thử đã hết hạn sử dụng!");
						return;
					}
				} catch (ParseException e) {
					alert.showErr("Phiên bản dùng thử đã hết hạn sử dụng!");
					return;
				}

			}
		});
	}

}

/*
 * (non-Java-doc)
 * 
 * @see java.lang.Object#Object()
 */
