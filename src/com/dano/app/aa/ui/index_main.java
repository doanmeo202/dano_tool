package com.dano.app.aa.ui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.filechooser.FileSystemView;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.poi.EncryptedDocumentException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.dano.app.aa.component.app_component;
import com.dano.app.lib.DATE;
import com.dano.app.lib.LIB_EXCEL;
import com.dano.app.lib.SELENIUM;

public class index_main extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4661786408203581226L;
	/**
	 * 
	 */
	private JTextField urlTextField;
	private JTextField quantityTextField;

	public app_component cpn;
	private List<Map<Integer, Object>> listData;
	private List<Map<Integer, Object>> listDataAnswerTrue;

	public String getName() {
		return this.getClass().getSimpleName().toLowerCase();
	}

	String textfile;

	public index_main() {
		// TODO Auto-generated constructor stub
		// Thiết lập các thuộc tính cho cửa sổ giao diện

		setSize(500, 300);

		JPanel mainPanel = new JPanel(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.insets = new Insets(5, 5, 5, 5);

		// Box chọn file
		JLabel fileLabel = new JLabel("Chọn File:");
		JButton fileButton = new JButton("Browse");
		constraints.gridx = 0;
		constraints.gridy = 0;
		constraints.anchor = GridBagConstraints.WEST;
		mainPanel.add(fileLabel, constraints);

		constraints.gridx = 1;
		constraints.gridy = 0;
		constraints.anchor = GridBagConstraints.WEST;
		mainPanel.add(fileButton, constraints);

		// Input text nhập URL
		JLabel urlLabel = new JLabel("Đường dẫn bài thi:");
		urlTextField = new JTextField(20);
		urlTextField.setText(
				"https://docs.google.com/forms/d/e/1FAIpQLSeAYzd-_1NPanKF-spkIY9H0czz8v9Kq9O0wrJsEjxm86OMjw/viewform?usp=sf_link");
		constraints.gridx = 0;
		constraints.gridy = 1;
		constraints.anchor = GridBagConstraints.WEST;
		mainPanel.add(urlLabel, constraints);

		constraints.gridx = 1;
		constraints.gridy = 1;
		constraints.anchor = GridBagConstraints.WEST;
		mainPanel.add(urlTextField, constraints);

		// Input text nhập số lượng
		JLabel quantityLabel = new JLabel("Số lượng xử lý:");
		quantityTextField = new JTextField(20);
		constraints.gridx = 0;
		constraints.gridy = 2;
		constraints.anchor = GridBagConstraints.WEST;
		mainPanel.add(quantityLabel, constraints);

		constraints.gridx = 1;
		constraints.gridy = 2;
		constraints.anchor = GridBagConstraints.WEST;
		mainPanel.add(quantityTextField, constraints);

		// Checkboxes
		// JCheckBox checkBox1 = new JCheckBox("Chỉ điền thông tin và chọn đáp án");
		// JCheckBox checkBox2 = new JCheckBox("Hoàn thành bài thi và đóng trình
		// duyệt?");
//
//		constraints.gridx = 0;
//		constraints.gridy = 3;
//		constraints.gridwidth = 2;
//		constraints.anchor = GridBagConstraints.WEST;
//		mainPanel.add(checkBox1, constraints);
//
//		constraints.gridx = 0;
//		constraints.gridy = 4;
//		constraints.gridwidth = 2;
//		constraints.anchor = GridBagConstraints.WEST;
//		mainPanel.add(checkBox2, constraints);
//		constraints.gridx = 0;
//		constraints.gridy = 5;
//		constraints.gridwidth = 2;
//		constraints.anchor = GridBagConstraints.WEST;
//		mainPanel.add(checkBox2, constraints);
		// Nút bấm bắt đầu
		JButton startButton = new JButton("Bắt đầu");
		constraints.gridx = 0;
		constraints.gridy = 6;
		constraints.gridwidth = 2;
		constraints.anchor = GridBagConstraints.CENTER;
		mainPanel.add(startButton, constraints);
		JLabel alertLabel0 = new JLabel("[Notify]:");
		JLabel alertLabel1 = new JLabel("Phiên bản dùng thử [hết hạn 30/09/2023]");
		constraints.gridx = 0;
		constraints.gridy = 7;
		constraints.gridwidth = 2;
		constraints.anchor = GridBagConstraints.WEST;
		mainPanel.add(alertLabel0, constraints);

		constraints.gridx = 1;
		constraints.gridy = 7;
		constraints.gridwidth = 2;
		constraints.anchor = GridBagConstraints.WEST;
		mainPanel.add(alertLabel1, constraints);

		add(mainPanel);

		// Xử lý sự kiện khi nhấn nút bắt đầu
		startButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					String url = urlTextField.getText();
					String quantity = quantityTextField.getText();
					int listSize = listData.size();
					int quantityInt = Integer.parseInt(ObjectUtils.toString(quantity, "0"));
					boolean is_next = false;
					if (quantityInt > listSize) {
						quantityInt = listSize;
					}
					if (quantityInt != 0) {
						is_next = true;
					} else {
						int check = JOptionPane.showConfirmDialog(index_main.this,
								"Bạn đồng ý thực làm hết " + listSize + " dữ liệu?");
						if (check == 0) {
							quantityInt = listSize;
							is_next = true;
						}
						if (listSize > 0) {

						} else {
							JOptionPane.showMessageDialog(index_main.this,
									"Không có dữ liệu học viên, vui lòng kiểm tra file tải lên", "THÔNG BÁO",
									JOptionPane.INFORMATION_MESSAGE);
						}
					}
					if (is_next) {
						WebDriver driver = SELENIUM.openBrowser(url);
						running(quantityInt, driver);
					}
				} catch (Exception e2) {
					JOptionPane.showMessageDialog(index_main.this, "Lỏ mẹ rồi, Lỗi nè: " + e2.getMessage(), "THÔNG BÁO",
							JOptionPane.INFORMATION_MESSAGE);
				}

			}

			public void running(int quantityInt, WebDriver driver) {
				for (int i = 0; i < quantityInt; i++) {
					Map map = listData.get(i);

					if (SELENIUM.sleep(2000)) {
						List<WebElement> inputText = SELENIUM.inputText(driver);
						List<WebElement> inputDate = SELENIUM.inputDate(driver);
						List<WebElement> inputTextArea = SELENIUM.inputTextArea(driver);
						List<WebElement> button = SELENIUM.buttonSubmit(driver);
						WebElement nameStudent = inputText.get(0);
						WebElement codeStudent = inputText.get(1);
						WebElement birthdate = inputDate.get(0);
						WebElement position = inputTextArea.get(0);
						SELENIUM.setText(map.get(0).toString(), nameStudent);
						String date = map.get(1).toString().replace(".", "/");
						DATE datefmt = new DATE();
						SELENIUM.setText(datefmt.DMYtoMDY(date), birthdate);
						SELENIUM.setText(map.get(4).toString(), position);
						SELENIUM.setText(map.get(3).toString(), codeStudent);
						button.get(0).click();
						driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

						if (SELENIUM.sleep(2000)) {
							List<WebElement> answer = SELENIUM.ListAnswer(driver);
							for (Map<Integer, Object> mapAns : listDataAnswerTrue) {
								String ansTrue = mapAns.get(0).toString();
								for (WebElement webElement : answer) {
									String dataValue = webElement.getAttribute("data-value");
									if (ansTrue.equals(dataValue)) {
										webElement.click();
										break; // Thoát khỏi vòng lặp sau khi tìm thấy câu trả lời chính xác
									}
								}
							}

							// snapShotPicture(driver, i);
							// driver.quit();
						}
					}

				}
			}
		});
		fileButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				// Hiển thị hộp thoại chọn file
				JFileChooser fileChooser = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
				fileChooser.setDialogTitle("Chọn file");

				// Chỉ cho phép chọn file có định dạng txt

				int returnValue = fileChooser.showOpenDialog(null);
				if (returnValue == JFileChooser.APPROVE_OPTION) {
					// Người dùng đã chọn một file
					String selectedFile = fileChooser.getSelectedFile().getPath();
					try {
						LIB_EXCEL.setFileInput(selectedFile, 0);
						listData = LIB_EXCEL.SnapShortData();
						quantityTextField.setText("" + listData.size());
						LIB_EXCEL.setFileInput(selectedFile, 1);
						listDataAnswerTrue = LIB_EXCEL.SnapShortData();
						System.out.println(listDataAnswerTrue);
					} catch (EncryptedDocumentException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					// Hiển thị đường dẫn file trên giao diện
					System.out.println("Đã chọn file: " + selectedFile);
				}
			}
		});

	}

	public static void main(String[] args) {
		index_main example = new index_main();
		example.setVisible(true);
	}
}
