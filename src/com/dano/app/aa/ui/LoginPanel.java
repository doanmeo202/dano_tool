package com.dano.app.aa.ui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class LoginPanel extends JPanel {
	private JTextField usernameField;
	private JPasswordField passwordField;
	private String name;

	public String getName() {
		return this.getClass().getSimpleName().toLowerCase();
	}


	public LoginPanel() {
		setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.insets = new Insets(5, 5, 5, 5);

		JLabel usernameLabel = new JLabel("Username:");
		add(usernameLabel, gbc);

		gbc.gridx++;
		usernameField = new JTextField(20);
		add(usernameField, gbc);

		gbc.gridx = 0;
		gbc.gridy++;
		JLabel passwordLabel = new JLabel("Password:");
		add(passwordLabel, gbc);

		gbc.gridx++;
		passwordField = new JPasswordField(20);
		add(passwordField, gbc);

		gbc.gridx = 0;
		gbc.gridy++;
		gbc.gridwidth = 2;
		JButton loginButton = new JButton("Login");
		add(loginButton, gbc);

		loginButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				String username = usernameField.getText();
				char[] passwordChars = passwordField.getPassword();
				String password = new String(passwordChars);

				// TODO: Perform login logic here
				// For example, you can validate the username and password against a database

				// Clear the fields after login attempt
				usernameField.setText("");
				passwordField.setText("");
			}
		});
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				JFrame frame = new JFrame();
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame.setSize(300, 200);
				frame.setLocationRelativeTo(null);

				LoginPanel loginPanel = new LoginPanel();
				frame.add(loginPanel);

				frame.setVisible(true);
			}
		});
	}
}