package com.dano.app.aa.component;

import java.awt.CardLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import com.dano.app.aa.ui.LoginPanel;
import com.dano.app.aa.ui.Main;
import com.dano.app.aa.ui.index_main;
import com.dano.app.lib.STRING;

public abstract class app_component extends JFrame {
	public static JPanel contentPane;
	public static JPanel panel1;
	public static JPanel panel2;
	public static CardLayout cardLayout;
	public static List<JPanel> LIST_COMPONENT = new ArrayList<JPanel>();
	public JMenuBar menuBar;
	public JMenu[] Jmenu;
	public static Map<Integer, String> MENUMAP = new TreeMap<Integer, String>();

	static {
		MENUMAP.put(0, "Main");
		MENUMAP.put(1, "Thiết Đặt");
		MENUMAP.put(3, "Thông Tin");
		MENUMAP.put(2, "Tài Khoản");
		contentPane = new JPanel();
		contentPane.setLayout(new CardLayout());

	}

	public JMenuBar getMenus() {
		menuBar = new JMenuBar();
		ArrayList LIST_MENU = new ArrayList(MENUMAP.keySet());
		int int_size = LIST_MENU.size();
		boolean isAdd = true;
		Jmenu = new JMenu[int_size];
		for (int i = 0; i < int_size; i++) {

			String value = (String) MENUMAP.get(i);
			JMenu Menu = new JMenu(value);
			Jmenu[i] = Menu;
			menuBar.add(Menu);
		}
		return menuBar;
	}

	public void addSubMenu(int menuIndex, String submenuName, JPanel jpanel) {
		if (menuIndex >= 0 && menuIndex < Jmenu.length) {
			JMenu menu = Jmenu[menuIndex];
			JMenuItem submenu = new JMenuItem(submenuName);
			menu.add(submenu);
			String slug = STRING.getSlug(submenuName);
			addPanel(jpanel, STRING.getSlug(submenuName));
			submenu.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent arg0) {
					app_component.showPanel(slug);

				}

			});
		}
	}

	public static JPanel addPanel(JPanel component, String name) {
		app_component.contentPane.add(component, name);
		return app_component.contentPane;
	}

	public static void showPanel(String panel_name) {
		String lowkey = panel_name.toLowerCase();
		app_component.cardLayout = (CardLayout) contentPane.getLayout();
		app_component.cardLayout.show(contentPane, lowkey);
		System.out.println("return page: " + lowkey);

	}

}
