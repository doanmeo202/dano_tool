package com.dano.app.aa.component;

import javax.swing.JOptionPane;

public class alert extends app_component {
	private static String TITLE="THÔNG BÁO";
	private void showDiaLog(Object mess) {
		JOptionPane.showMessageDialog(this, mess, TITLE, JOptionPane.INFORMATION_MESSAGE);
	}

	public static void show(Object mess) {
		alert a = new alert();
		TITLE="THÔNG BÁO";
		a.showDiaLog(mess);
	}
	public static void showErr(Object mess) {
		alert a = new alert();
		TITLE="THẤT BẠI";
		a.showDiaLog(mess);
	}
}
