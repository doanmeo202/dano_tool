package com.dano.app.aa.component;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

import com.dano.app.aa.ui.LoginPanel;
import com.dano.app.aa.ui.index_main;

public class MENU extends app_component {
	private List<Map> LIST_MAP_MENU = new ArrayList<Map>();
	public Map<Integer, String> MENUS = new TreeMap<Integer, String>();
	public List<JMenuItem> LIST_MENU_ITEMS = new ArrayList<JMenuItem>();
	public ArrayList LIST_MENU;
	public JMenuBar menuBar;

	public void MENU() {
		addMenu("Tài Khoản");
		addSubMenu(0, "Đăng Nhập", new LoginPanel());
		addSubMenu(0, "Panel1", new index_main());
		
	}

	public void addMenu(String menuTitle) {
		LIST_MENU = new ArrayList(MENUS.keySet());
		int int_size = LIST_MENU.size();
		boolean isAdd = true;
		for (int i = 0; i < LIST_MENU.size(); i++) {

			String value = (String) MENUS.get(i);
			if (menuTitle.equals(value)) {
				isAdd = false;
			}
		}
		if (isAdd) {
			MENUS.put(int_size, menuTitle);
		}
	}

	public void addSubMenu(int index_menu, final String nameSubItem, JPanel classSimpleName) {
		Map menuItemMap = new HashMap();
		menuItemMap.put("menu", index_menu);
		menuItemMap.put("item_menu", nameSubItem);

		menuItemMap.put("item", classSimpleName);
		final String NAMENE = classSimpleName.getName().toLowerCase();
		JMenuItem menuItem = new JMenuItem(nameSubItem);
		contentPane.add(classSimpleName, classSimpleName.getName().toLowerCase());
		menuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				showPanel(NAMENE.toLowerCase());
			}
		});
		menuItemMap.put("menuItem", menuItem);
		LIST_MAP_MENU.add(menuItemMap);
		LIST_MENU = new ArrayList(MENUS.keySet());
		this.menuBar = new JMenuBar();
		for (int i = 0; i < LIST_MENU.size(); i++) {

			String value = (String) MENUS.get(i);
			JMenu Menu = new JMenu(value);
			for (Map map : LIST_MAP_MENU) {
				int int_menu = Integer.parseInt(map.get("menu").toString());
				if (i == int_menu) {
					JMenuItem item = (JMenuItem) map.get("menuItem");
					Menu.add(item);
				}

			}
			this.menuBar.add(Menu);

		}
	}

	public static void main(String[] args) {

	}

}
