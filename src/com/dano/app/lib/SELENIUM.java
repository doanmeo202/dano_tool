package com.dano.app.lib;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.poi.EncryptedDocumentException;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class SELENIUM {
	private static int TIME_DEFAULT = 2000;

	public SELENIUM() {

	}

	public static void setText(String text, WebElement el) {
		if (el.isDisplayed()) {
			el.sendKeys(text);
		}
	}

	public synchronized static boolean sleep() {
		try {
			Thread.sleep(TIME_DEFAULT);
			return true;
		} catch (InterruptedException e) {
			return false;
		}

	}

	public synchronized static boolean sleep(int time) {
		try {
			Thread.sleep(time);
			return true;
		} catch (InterruptedException e) {
			return false;
		}

	}

	public synchronized static WebElement getElement(List<WebElement> list, int position) {
		if (list.size() < position) {
			return null;
		}
		return list.get(position);
	}

	public static synchronized List<WebElement> inputText(WebDriver driver) {
		return driver.findElements(By.cssSelector("input[type='text']"));
	}

	public static synchronized List<WebElement> inputDate(WebDriver driver) {
		return driver.findElements(By.cssSelector("input[type='date']"));
	}

	public static synchronized List<WebElement> inputTextArea(WebDriver driver) {
		return driver.findElements(By.cssSelector("textarea"));
	}

	public static synchronized List<WebElement> buttonSubmit(WebDriver driver) {
		return driver.findElements(By.cssSelector("div[role='button']"));
	}

	public static synchronized List<WebElement> ListAnswer(WebDriver driver) {
		return driver.findElements(By.cssSelector("div[role='radio']"));
	}

	public synchronized static WebDriver openBrowser(String url) {
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--incognito");
		System.setProperty("webdriver.chrome.driver", ".\\chromedriver\\driver\\win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver(options);
		driver.get(url);
		sleep();
		return driver;
	}

	public static synchronized void snapShotPicture(WebDriver driver, int index) {
		File screenshotFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		try {
			// Lưu ảnh chụp vào đường dẫn mong muốn
			File destinationFile = new File("E:/file" + index + ".png");
			Files.copy(screenshotFile.toPath(), destinationFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
			System.out.println("Screenshot saved to: " + destinationFile.getAbsolutePath());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {

		try {
			LIB_EXCEL.setFileInput("D:/Test.xlsx", 0);
			List<Map<Integer, Object>> li = LIB_EXCEL.SnapShortData();
			for (int i = 0; i < 1; i++) {
				ChromeOptions options = new ChromeOptions();
				options.addArguments("--incognito");
				System.setProperty("webdriver.chrome.driver", ".\\chromedriver\\driver\\win32\\chromedriver.exe");
				WebDriver driver = new ChromeDriver(options);
//				driver.get(
//						"https://docs.google.com/forms/d/e/1FAIpQLSesQdRR0TBWkpXDVlRwt8Ns3rBm7pAuTMNEM8HODoEEX55yDA/viewform");

				driver.get(
						"https://docs.google.com/forms/d/e/1FAIpQLSeAYzd-_1NPanKF-spkIY9H0czz8v9Kq9O0wrJsEjxm86OMjw/viewform?usp=sf_link"); // thanh
				Map map = li.get(i);
				if (sleep(2000)) {
					List<WebElement> inputText = inputText(driver);
					List<WebElement> inputDate = inputDate(driver);
					List<WebElement> inputTextArea = inputTextArea(driver);
					List<WebElement> button = buttonSubmit(driver);
					WebElement nameStudent = inputText.get(0);
					WebElement codeStudent = inputText.get(1);
					WebElement birthdate = inputDate.get(0);
					WebElement position = inputTextArea.get(0);
					setText(map.get(0).toString(), nameStudent);
					String date = map.get(1).toString().replace(".", "/");
					DATE datefmt = new DATE();
					setText(datefmt.DMYtoMDY(date), birthdate);
					setText(map.get(4).toString(), position);
					setText(map.get(3).toString(), codeStudent);
					button.get(0).click();
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

					if (sleep(2000)) {
						List<WebElement> answer = ListAnswer(driver);
						for (WebElement webElement : answer) {
							String dataValue = webElement.getAttribute("data-value");
							if (dataValue.equals("Đáp án chính xác của câu 2")) {
								webElement.click();
							}
						}
						// snapShotPicture(driver, i);
						// driver.quit();
					}
				}

			}
		} catch (EncryptedDocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// Đóng trình duyệt

		// driver.quit();
	}
}
