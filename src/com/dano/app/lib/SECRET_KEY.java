package com.dano.app.lib;

import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

public class SECRET_KEY {
	private static final String ENCRYPTION_ALGORITHM = "AES";

	// Phương thức để tạo khóa bí mật
	public SecretKey generateSecretKey() throws NoSuchAlgorithmException {
		KeyGenerator keyGenerator = KeyGenerator.getInstance(ENCRYPTION_ALGORITHM);
		keyGenerator.init(128); // Kích thước khóa 128-bit
		return keyGenerator.generateKey();
	}

	// Phương thức để mã hóa thông tin bản quyền
	public String encryptLicenseInfo(String licenseInfo, SecretKey secretKey) throws NoSuchPaddingException,
			NoSuchAlgorithmException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
		Cipher cipher = Cipher.getInstance(ENCRYPTION_ALGORITHM);
		cipher.init(Cipher.ENCRYPT_MODE, secretKey);
		byte[] encryptedBytes = cipher.doFinal(licenseInfo.getBytes());
		return new String(encryptedBytes);
	}

	// Phương thức để giải mã mã bản quyền
	public String decryptLicenseCode(String licenseCode, SecretKey secretKey) throws NoSuchPaddingException,
			NoSuchAlgorithmException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
		Cipher cipher = Cipher.getInstance(ENCRYPTION_ALGORITHM);
		cipher.init(Cipher.DECRYPT_MODE, secretKey);
		byte[] decryptedBytes = cipher.doFinal(licenseCode.getBytes());
		return new String(decryptedBytes);
	}

	// Phương thức để kiểm tra tính hợp lệ của khóa bản quyền (ví dụ: kiểm tra thời
	// hạn)
	public boolean checkLicenseValidity(String decryptedInfo) {
		// Thực hiện kiểm tra thông tin bản quyền, ví dụ: kiểm tra thời hạn, chữ ký,
		// v.v.
		// Trả về true nếu khóa bản quyền hợp lệ, ngược lại trả về false
		// Ví dụ:
		// return expirationDate.after(new Date()); // Kiểm tra xem thời hạn có sau ngày
		// hiện tại hay không
		return true; // Mặc định trả về true trong ví dụ này
	}
}
