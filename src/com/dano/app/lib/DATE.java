package com.dano.app.lib;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DATE {

	private Date date;
	private Calendar calendar;
	private SimpleDateFormat fmt_timestamp;
	private SimpleDateFormat fmt_work_ym; // YYYYMM
	private SimpleDateFormat fmt_sql_date; // YYYY-MM-dd
	private SimpleDateFormat fmt_sdf;
	private SimpleDateFormat fmt_MMddyyyy;
	private SimpleDateFormat fmt_ddMMyyyy;
	private String timestamp;
	private String sqlDate;

	public String getSqlDate() {
		return sqlDate;
	}

	public void setSqlDate(String sqlDate) {
		this.sqlDate = sqlDate;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public DATE() {
		fmt_timestamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		fmt_work_ym = new SimpleDateFormat("yyyyMM");
		fmt_sql_date = new SimpleDateFormat("yyyy-MM-dd");
		fmt_MMddyyyy = new SimpleDateFormat("MM/dd/yyyy");
		fmt_ddMMyyyy = new SimpleDateFormat("dd/MM/yyyy");
		calendar = Calendar.getInstance();
	}

	public String DMYtoMDY(String ddMMyyyy) {
		try {
			return fmt_MMddyyyy.format(fmt_ddMMyyyy.parse(ddMMyyyy));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public Date getDate(String timestamp) {
		try {
			date = fmt_timestamp.parse(timestamp);
		} catch (ParseException e) {
		
			e.printStackTrace();
		}
		return date;
	}

	public String getCurrentTimestamp() {
		return fmt_timestamp.format(new Date());
	}

	public String getTimestamp(Date date) {
		return fmt_timestamp.format(date);
	}

	public String addMinutesTimeStamp(String date, int minutes) {
		calendar.setTime(getDate(date));
		calendar.add(Calendar.MINUTE, minutes);
		Date newDate = calendar.getTime();
		return fmt_timestamp.format(newDate);

	}

	public String addHourTimeStamp(String date, int hour) {
		calendar.setTime(getDate(date));
		calendar.add(Calendar.HOUR, hour);
		Date newDate = calendar.getTime();
		return fmt_timestamp.format(newDate);

	}

	public String addDateTimeStamp(String date, int day) {

		calendar.setTime(getDate(date));
		calendar.add(Calendar.DAY_OF_MONTH, day);
		Date newDate = calendar.getTime();
		return fmt_timestamp.format(newDate);

	}

	public int getSecondFromTimestamp() throws ParseException {
		if (timestamp == null || timestamp.isEmpty()) {
			throw new ParseException("timestamp is empty", 0);
		}
		date = fmt_timestamp.parse(timestamp);

		calendar.setTime(date);
		return calendar.get(Calendar.SECOND);
	}

	public int getMinuteFromTimestamp() throws ParseException {
		if (timestamp == null || timestamp.isEmpty()) {
			throw new ParseException("timestamp is empty", 0);
		}
		date = fmt_timestamp.parse(timestamp);

		calendar.setTime(date);
		return calendar.get(Calendar.MINUTE);
	}

	public int getHourFromTimestamp() throws ParseException {
		if (timestamp == null || timestamp.isEmpty()) {
			throw new ParseException("timestamp is empty", 0);
		}
		date = fmt_timestamp.parse(timestamp);

		calendar.setTime(date);
		return calendar.get(Calendar.HOUR_OF_DAY);
	}

	public String getCurrentTimeSql() {
		return fmt_sql_date.format(new Date());
	}

	public String getCurrentWKM() {
		return fmt_work_ym.format(new Date());
	}

	public int getDayOfMonthFromSqlDate() throws ParseException {
		if (sqlDate == null || sqlDate.isEmpty()) {
			throw new ParseException("sqlDate is empty", 0);
		}
		date = fmt_sql_date.parse(sqlDate);

		calendar.setTime(date);
		return calendar.get(Calendar.DAY_OF_MONTH);
	}

	public int getMonthFromSqlDate() throws ParseException {
		if (sqlDate == null || sqlDate.isEmpty()) {
			throw new ParseException("sqlDate is empty", 0);
		}
		date = fmt_sql_date.parse(sqlDate);
		calendar.setTime(date);
		return calendar.get(Calendar.MONTH) + 1; // Tháng trong Calendar bắt đầu từ 0
	}

	public int getYearFromSqlDate() throws ParseException {
		if (sqlDate == null || sqlDate.isEmpty()) {
			throw new ParseException("sqlDate is empty", 0);
		}
		date = fmt_sql_date.parse(sqlDate);

		calendar.setTime(date);
		return calendar.get(Calendar.YEAR);
	}

	public Calendar getCalendarFromDate(String dateString, String dateFormat) throws ParseException {
		fmt_sdf = new SimpleDateFormat(dateFormat);
		date = fmt_sdf.parse(dateString);
		calendar.setTime(date);
		return calendar;
	}

	public boolean checkTimeEvent(String timeBegin, String timeStop) throws ParseException {
		Date currentTime = getDate(getCurrentTimestamp());
		Date startTime = getDate(timeBegin);
		Date endTime = getDate(timeStop);
		System.out.println(getCurrentTimestamp());
		System.out.println(timeBegin);
		System.out.println(timeStop);
		return currentTime.after(startTime) && currentTime.before(endTime);
	}

	public static void main(String[] args) {
		DATE d = new DATE();
		System.out.println(d.addDateTimeStamp(d.getCurrentTimestamp(), 10));
	}
}
