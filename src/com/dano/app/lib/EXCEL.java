package com.dano.app.lib;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

public class EXCEL {
	public static void main(String[] args) {
		try {
			// Đường dẫn đến file Excel
			String filePath = "D:\\Test.xlsx";

			// Tạo đối tượng FileInputStream để đọc file
			FileInputStream fis = new FileInputStream(new File(filePath));

			// Tạo đối tượng Workbook từ FileInputStream
			Workbook workbook = WorkbookFactory.create(fis);

			// Lấy ra sheet đầu tiên từ Workbook
			Sheet sheet = workbook.getSheetAt(0);

			// Lấy ra dòng đầu tiên từ sheet
			Row headerRow = sheet.getRow(0);

			// Tạo danh sách lưu trữ dữ liệu
			List<Map<String, Object>> data = new ArrayList();

			// Duyệt qua từng dòng trong sheet (bỏ qua dòng header)
			for (int rowIndex = 1; rowIndex <= sheet.getLastRowNum(); rowIndex++) {
				Row row = sheet.getRow(rowIndex);

				// Tạo đối tượng Map để lưu trữ dữ liệu của mỗi dòng
				Map<String, Object> rowData = new HashMap();

				// Duyệt qua từng ô trong dòng
				for (int cellIndex = 0; cellIndex < headerRow.getLastCellNum(); cellIndex++) {
					Cell cell = row.getCell(cellIndex);

					String columnName = "col" + cellIndex;
					// Kiểm tra kiểu dữ liệu của ô
					if (cell != null) {

						if (cell.getCellType() == CellType.NUMERIC) {
							rowData.put(columnName, cell.getNumericCellValue());
						} else if (cell.getCellType() == CellType.STRING) {
							rowData.put(columnName, cell.getStringCellValue());
						} else if (cell.getCellType() == CellType.BOOLEAN) {
							rowData.put(columnName, cell.getBooleanCellValue());
						} else if (cell.getCellType() == CellType.BLANK) {
							rowData.put(columnName, ""); // Xử lý ô trống
						} else {
							rowData.put(columnName, null); // Xử lý các kiểu dữ liệu khác
						}
					} else {
						rowData.put(columnName, null); // Xử lý ô null
					}
				}

				// Thêm dữ liệu của mỗi dòng vào danh sách data
				data.add(rowData);
			}

			// In ra danh sách data
			for (Map<String, Object> rowData : data) {
				for (Map.Entry<String, Object> entry : rowData.entrySet()) {
					System.out.println(entry.getKey() + ": " + entry.getValue());
				}
				System.out.println("----------------------");
			}

			// Đóng FileInputStream và Workbook sau khi sử dụng xong
			workbook.close();
			fis.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
