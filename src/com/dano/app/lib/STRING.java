package com.dano.app.lib;

import java.text.Normalizer;

public class STRING {

	private static String get_slug(String input) {
		String cleanedInput = input.replaceAll("[^a-zA-Z0-9\\s]", "");
		return cleanedInput.toLowerCase().replaceAll("\\s", "-");
	}

	public static String getSlug(String input) {
		return get_slug(Normalizer.normalize(input.replaceAll("đ", "d"), Normalizer.Form.NFD)
				.replaceAll("[^\\p{ASCII}\\w\\s-]", "").replaceAll("[-]+", "-").toLowerCase());
	}

	
	
	public static void main(String[] conargs) {
		System.out.println(getSlug("Bài thực hành số 1"));
	}
}
