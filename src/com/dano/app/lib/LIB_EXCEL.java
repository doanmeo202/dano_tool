package com.dano.app.lib;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

public class LIB_EXCEL {
	private static File fileInput;
	private static Workbook workbook;
	private static FileInputStream fis;
	private static Sheet sheet;
	private static List<Map<Integer, Object>> DATA;

	private static File getFileInput() {
		return fileInput;
	}

	public static synchronized void setFileInput(File file_input, int sheet_)
			throws EncryptedDocumentException, IOException {
		fileInput = file_input;
		fis = new FileInputStream(getFileInput());
		workbook = WorkbookFactory.create(fis);
		workbook.getSpreadsheetVersion();
		sheet = getSheet(sheet_);
	}

	public static synchronized void setFileInput(String file_input, int sheet_)
			throws EncryptedDocumentException, IOException {
		fileInput = new File(file_input);
		fis = new FileInputStream(getFileInput());

		workbook = WorkbookFactory.create(fis);
		workbook.getSpreadsheetVersion();
		sheet = getSheet(sheet_);
	}

	public static Sheet getSheet(int sheet_) {
		sheet = workbook.getSheetAt(sheet_);
		return sheet;
	}

	public static Row getRow(int row) {
		return sheet.getRow(row);
	}

	public synchronized static List<Map<Integer, Object>> SnapShortData() {
		DATA = new ArrayList();
		Row headerRow = getRow(0);
		for (int rowIndex = 0; rowIndex <= sheet.getLastRowNum(); rowIndex++) {
			Row row = getRow(rowIndex);

			Map<Integer, Object> rowData = new TreeMap<Integer, Object>();
			for (int cellIndex = 0; cellIndex < headerRow.getLastCellNum(); cellIndex++) {
				Cell cell = row.getCell(cellIndex);
				if (cell != null) {

					if (cell.getCellType() == CellType.NUMERIC) {
						rowData.put(cellIndex, cell.getNumericCellValue());
					} else if (cell.getCellType() == CellType.STRING) {
						rowData.put(cellIndex, cell.getStringCellValue());
					} else if (cell.getCellType() == CellType.BOOLEAN) {
						rowData.put(cellIndex, cell.getBooleanCellValue());
					} else if (cell.getCellType() == CellType.BLANK) {
						rowData.put(cellIndex, ""); // Xử lý ô trống
					} else {
						rowData.put(cellIndex, null); // Xử lý các kiểu dữ liệu khác
					}
				} else {
					rowData.put(cellIndex, null); // Xử lý ô null
				}
			}
			DATA.add(rowData);
		}
		return DATA;

	}

	public static void main(String[] args) {
		SELENIUM.openBrowser(
				"https://docs.google.com/forms/d/e/1FAIpQLSesQdRR0TBWkpXDVlRwt8Ns3rBm7pAuTMNEM8HODoEEX55yDA/viewform");
		SELENIUM.openBrowser(
				"https://docs.google.com/forms/d/e/1FAIpQLSesQdRR0TBWkpXDVlRwt8Ns3rBm7pAuTMNEM8HODoEEX55yDA/viewform");
		SELENIUM.openBrowser(
				"https://docs.google.com/forms/d/e/1FAIpQLSesQdRR0TBWkpXDVlRwt8Ns3rBm7pAuTMNEM8HODoEEX55yDA/viewform");
		SELENIUM.openBrowser(
				"https://docs.google.com/forms/d/e/1FAIpQLSesQdRR0TBWkpXDVlRwt8Ns3rBm7pAuTMNEM8HODoEEX55yDA/viewform");
		
	}

}
