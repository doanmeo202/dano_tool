package com.dano.app.lib;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class SeleniumMultipleTabsExample {
	public static void main(String[] args) {
		// Số lượng tab cần mở
		int numTabs = 4;

		// Khởi tạo trình duyệt Chrome với cấu hình tùy chỉnh
		System.setProperty("webdriver.chrome.driver", ".\\chromedriver\\driver\\win32\\chromedriver.exe");
		WebDriver driver = createCustomChromeDriver();

		// Mở các tab và xếp chúng theo lưới
		openAndArrangeTabs(driver, numTabs);

		// Đóng trình duyệt
		driver.quit();
	}

	private static WebDriver createCustomChromeDriver() {
		// Khởi tạo ChromeOptions để cấu hình trình duyệt
		ChromeOptions options = new ChromeOptions();
		// Xếp các tab theo lưới trên màn hình
		options.addArguments("--enable-features=TabGridLayout");
		options.addArguments("--force-tab-tiling");

		// Khởi tạo trình duyệt Chrome với ChromeOptions
		WebDriver driver = new ChromeDriver(options);

		// Di chuyển cửa sổ đến góc trên cùng bên trái của màn hình
		Point windowPosition = new Point(0, 0);
		driver.manage().window().setPosition(windowPosition);

		return driver;
	}

	private static void openAndArrangeTabs(WebDriver driver, int numTabs) {
		// Tạo danh sách các tab mở
		List<WebDriver> tabs = new ArrayList();

		// Mở các tab và thêm vào danh sách
		for (int i = 0; i < numTabs; i++) {
			WebDriver tab = new ChromeDriver();
			tabs.add(tab);
		}

		// Xếp các tab theo lưới trên màn hình
		arrangeTabsInGrid(tabs);

		// Đóng các tab
		for (WebDriver tab : tabs) {
			//tab.quit();
		}
	}

	private static void arrangeTabsInGrid(List<WebDriver> tabs) {
		// Lấy kích thước màn hình
		// java.awt.Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		Dimension screenSize = new Dimension(300, 300);
		int screenWidth = (int) screenSize.getWidth();
		int screenHeight = (int) screenSize.getHeight();

		// Tính toán kích thước và vị trí của từng tab
		int gridCols = (int) Math.ceil(Math.sqrt(tabs.size()));
		int gridRows = (int) Math.ceil((double) tabs.size() / gridCols);
		int tabWidth = screenWidth / gridCols;
		int tabHeight = screenHeight / gridRows;

		int tabIdx = 0;
		for (int row = 0; row < gridRows; row++) {
			for (int col = 0; col < gridCols; col++) {
				if (tabIdx >= tabs.size()) {
					break;
				}

				WebDriver tab = tabs.get(tabIdx);
				Point tabPosition = new Point(col * tabWidth, row * tabHeight);
				tab.manage().window().setPosition(tabPosition);
				tab.manage().window().setSize(new Dimension(tabWidth, tabHeight));

				tabIdx++;
			}
		}
	}
}