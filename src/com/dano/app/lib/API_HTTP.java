package com.dano.app.lib;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;

/*@auth: NgocThanhDoan
 * */
public class API_HTTP {
	private Map<String, String> mapRequestProperty = new HashMap<String, String>();
	private Map<String, Object> mapRequestBody = new HashMap<String, Object>();
	private URL urlSend;
	private HttpURLConnection connection;
	private int responseCode;
	private ObjectMapper objectMapper;
	private Object parsedObject;
	private List<Map<String, Object>> listMap;
	private String requestBodyString;
	private StringBuilder responseBody;
	private int timeoutInMillis = 5000;
	private boolean is_log = true;

	public void offLog() {
		this.is_log = false;
	}

	public int getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(int responseCode) {
		this.responseCode = responseCode;
	}

	public void setRequestBodyString(String jsonData) {
		this.requestBodyString = jsonData;
	}

	public void setRequestBody(String key, String value) {
		mapRequestBody.put(key, value);
	}

	public void setRequestBody(Map map) {
		mapRequestBody.putAll(map);

	}

	public void setMethod(String Method) {
		mapRequestProperty.put("Method", Method);
	}

	public void setRequestProperty(String key, String value) {
		mapRequestProperty.put(key, value);
	}

	public String getMethod() {
		return mapRequestProperty.get("Method");
	}

	public API_HTTP() {
		mapRequestProperty.put("Content-Type", "application/json");
		mapRequestProperty.put("Method", "GET");
		objectMapper = new ObjectMapper();
		requestBodyString = "";
	}

	public List<Map<String, Object>> callApi(String url) throws Exception {
		try {
			sendHttpReturnMap(url, getMethod(), requestBodyString, mapRequestBody, new Callback() {

				public void onTimeout() throws Exception {
					throw new Exception("API TIME OUT CONNECTION!!!!");

				}

				public void onSuccess(String response) {
				
				}
			});
		} catch (Exception e) {
			throw e;
		}

		return listMap;
	}

	public void call(String url) throws Exception {
		sendHttpReturnMap(url, getMethod(), requestBodyString, mapRequestBody, null);

	}

	

	public Map<String, Object> callApiMap(String url) throws Exception {
		sendHttpReturnMap(url, getMethod(), requestBodyString, mapRequestBody, new Callback() {

			public void onTimeout() throws Exception {
				throw new Exception("API TIME OUT CONNECTION!!!!");

			}

			public void onSuccess(String response) {
				// TODO Auto-generated method stub

			}
		});

		Map map = new HashMap();
		if (listMap != null) {
			for (Map<String, Object> map1 : listMap) {
				map = map1;
			}
		}
		return map;
	}

	public void sendHttpReturnMap(String url, String method, String requestBody, Map requestBodyMap) throws Exception {
		try {
			urlSend = new URL(url);
			connection = (HttpURLConnection) urlSend.openConnection();
			connection.setRequestMethod(method);
			connection.setRequestProperty("Content-Type", "application/json");
			ArrayList PropertyList = new ArrayList(mapRequestProperty.keySet());

			for (int i = 0; i < PropertyList.size(); i++) {
				String key = (String) PropertyList.get(i);
				String value = (String) mapRequestProperty.get(PropertyList.get(i));
				if (!key.equals("Method")) {
					connection.setRequestProperty(key, value);
				}

			}
			connection.setDoOutput(true);

			// Send the request body
			if (!"".equals(requestBody)) {
				byte[] requestBodyBytes = requestBody.getBytes(StandardCharsets.UTF_8);
				connection.getOutputStream().write(requestBodyBytes);
			}
			ArrayList myKeyList = new ArrayList(requestBodyMap.keySet());
			if (myKeyList.size() > 0) {
				byte[] requestBodyBytes = objectMapper.writeValueAsString(requestBodyMap)
						.getBytes(StandardCharsets.UTF_8);
				connection.getOutputStream().write(requestBodyBytes);
			}

			int responseCode = connection.getResponseCode();
			setResponseCode(responseCode);

			if (responseCode == HttpURLConnection.HTTP_OK) {
				BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
				String line;
				responseBody = new StringBuilder();
				while ((line = reader.readLine()) != null) {
					responseBody.append(line);
				}
				reader.close();
				parsedObject = objectMapper.readValue(responseBody.toString(), Object.class);
				listMap = new ArrayList<Map<String, Object>>();
				if (parsedObject instanceof List) {
					listMap = (List<Map<String, Object>>) parsedObject;
				} else if (parsedObject instanceof Map) {
					Map<String, Object> map = (Map<String, Object>) parsedObject;
					listMap.add(map);
				} else {

				}
			
			} else {
				System.out.println("No data due to error code: " + responseCode);
				throw new Exception("No data due to error code: " + responseCode);
			}

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Call failed");
			throw e;
		}

	}

	public void setTimeOut(int timeOut) {
		this.timeoutInMillis = timeOut;
	}

	public void sendHttpReturnMap(String url, String method, String requestBody, Map<String, Object> requestBodyMap,
			Callback callback) throws Exception {
		try {
			urlSend = new URL(url);
			connection = (HttpURLConnection) urlSend.openConnection();
			connection.setRequestMethod(method);
			connection.setRequestProperty("Content-Type", "application/json");
			ArrayList PropertyList = new ArrayList(mapRequestProperty.keySet());

			for (int i = 0; i < PropertyList.size(); i++) {
				String key = (String) PropertyList.get(i);
				String value = (String) mapRequestProperty.get(PropertyList.get(i));
				if (!key.equals("Method")) {
					if (is_log) {
						System.out.println("connection.setRequestProperty " + key + " " + value);
					}
					connection.setRequestProperty(key, value);
				}

			}
			connection.setDoOutput(true);

			// Set timeout
			connection.setConnectTimeout(timeoutInMillis);
			connection.setReadTimeout(timeoutInMillis);

			// Send the request body
			if (!"".equals(requestBody)) {
				byte[] requestBodyBytes = requestBody.getBytes(StandardCharsets.UTF_8);
				connection.getOutputStream().write(requestBodyBytes);
			}
			ArrayList myKeyList = new ArrayList(requestBodyMap.keySet());
			if (myKeyList.size() > 0) {
				byte[] requestBodyBytes = objectMapper.writeValueAsString(requestBodyMap)
						.getBytes(StandardCharsets.UTF_8);
				connection.getOutputStream().write(requestBodyBytes);
			}

			int responseCode = connection.getResponseCode();
			setResponseCode(responseCode);

			if (responseCode == HttpURLConnection.HTTP_OK) {
				BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
				String line;
				responseBody = new StringBuilder();
				while ((line = reader.readLine()) != null) {
					responseBody.append(line);
				}
				reader.close();
				try {
					parsedObject = objectMapper.readValue(responseBody.toString(), Object.class);
				} catch (Exception e) {
					parsedObject = responseBody;
				}
				listMap = new ArrayList<Map<String, Object>>();
				if (parsedObject instanceof List) {
					listMap = (List<Map<String, Object>>) parsedObject;
				} else if (parsedObject instanceof Map) {
					Map<String, Object> map = (Map<String, Object>) parsedObject;
					listMap.add(map);
				} else {

				}
				if (callback != null) {
					callback.onSuccess(responseBody.toString());
				}
			} else {
				throw new Exception("No data due to error code: " + responseCode);
			}

		} catch (SocketTimeoutException e) {
			if (callback != null) {
				callback.onTimeout();
			}
			throw e;
		} catch (Exception e) {
			if (is_log) {
				e.printStackTrace();
				System.out.println("Call failed");
			}
			throw e;
		}
	}

	public interface Callback {
		void onSuccess(String response);

		void onTimeout() throws Exception;
	}

	public String getStringData() {
		return responseBody.toString();
	}

	public static void main(String[] args) {
		API_HTTP api = new API_HTTP();

		try {
			// api.setTimeOut(5000);
			api.call("https://api.ipify.org");
			System.out.println(api.getStringData());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
