package com.dano.app.lib;

import java.awt.AWTException;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class CAPTURE {
	private Robot robot;
	private Rectangle screenRect;
	private BufferedImage screenCapture;
	private File file;

	public CAPTURE() {
		try {
			robot = new Robot();
		} catch (AWTException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public boolean wirteFile(BufferedImage image, String type, String filePath) {
		try {
			ImageIO.write(image, type, new File(filePath));
			return true;
		} catch (IOException e) {
			return false;
		}
	}

	public static void main(String[] args) {
		try {
			// Tạo đối tượng Robot để chụp màn hình
			Robot robot = new Robot();

			// Lấy kích thước màn hình
			Rectangle screenRect = new Rectangle(Toolkit.getDefaultToolkit().getScreenSize());

			// Chụp màn hình thành BufferedImage
			BufferedImage screenCapture = robot.createScreenCapture(screenRect);

			// Đường dẫn và tên file để lưu ảnh
			String filePath = "D:/screenshot.png";

			// Lưu ảnh thành file
			File file = new File(filePath);
			ImageIO.write(screenCapture, "png", file);

			System.out.println("Chụp màn hình thành công. Đã lưu ảnh tại: " + filePath);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
