package com.dano.app.lib;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class CONFIG {
	private static Map mapSetField = new HashMap();

	public static void clearProperties() {
		mapSetField.clear();
	}

	public static void addProperties(String key, Object value) {
		mapSetField.put(key, value);
	}

	public static Properties getPropertiesFile(String filePath) {
		Properties props = new Properties();
		FileInputStream fis;
		try {
			fis = new FileInputStream(filePath);
			props.load(fis);
		} catch (IOException e) {
			e.printStackTrace();
			System.err.println("PathFileConfig :" + "file not found!");
			return null;
		}
		return props;
	}

	public static void savePropertiesFile(String fileName) {
		savePropertiesFile(mapSetField, "", fileName);
	}

	public static void savePropertiesFile(String desc, String fileName) {
		savePropertiesFile(mapSetField, desc, fileName);
	}

	public static void savePropertiesFile(Map propertiesValue, String desc, String fileName) {
		ArrayList myKeyList = new ArrayList(propertiesValue.keySet());
		Properties props = new Properties();
		for (int i = 0; i < myKeyList.size(); i++) {
			String key = (String) myKeyList.get(i);
			String value = (String) propertiesValue.get(myKeyList.get(i));
			props.setProperty(key, value);
		}
		try {
			FileOutputStream fos = new FileOutputStream(fileName + ".properties");
			File file = new File(fileName + ".properties");
			props.setProperty("filePath", file.getAbsolutePath());
			props.store(fos, desc);
			System.out.println("saved!!! " + file.getAbsolutePath());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static Properties getdata() {
		return getPropertiesFile("config.properties");
	}

	public static Object getdata(String key) {
		return getdata().get(key);
	}

	public static void main(String[] args) {
		CONFIG.addProperties("title", "Author: webdano.com");

		CONFIG.savePropertiesFile("config");
		System.out.println(getdata());

		// pr.savePropertiesFile(map, "jbdcConfig", "jbdcConfig");

	}

}
